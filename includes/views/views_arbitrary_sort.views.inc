<?php

/**
 * @file
 * Provide views data and handlers for views_arbitrary_sort.module.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_views_data_alter().
 */
function views_arbitrary_sort_views_data_alter(&$data) {
  foreach ($data as $table => $table_data) {
    foreach ($table_data as $column => $value) {
      // Add a new handler wherever views_handler_sort is applicable.
      if (is_array($value) && array_key_exists('sort', $value) && $value['sort']['handler'] == 'views_handler_sort') {
        $original_group = 'No Group';

        // Get original group that the field belongs to.
        if (isset($data[$table][$column]['group'])) {
          $original_group = $data[$table][$column]['group'];
        }
        elseif (isset($data[$table]['table']['group'])) {
          $original_group = $data[$table]['table']['group'];
        }

        // Add new handler info.
        $data[$table]['arbitrary_sort_' . $column] = array(
          'real field' => $column,
          'group' => t('Arbitrary sort'),
          'title' => t('(@group) @title', array(
            '@group' => $original_group,
            '@title' => $data[$table][$column]['title'],
          )),
          'sort' => $data[$table][$column]['sort'],
        );

        // Reuse original help text.
        if (isset($data[$table][$column]['help'])) {
          $data[$table]['arbitrary_sort_' . $column]['help'] = t('@help', array(
            '@help' => $data[$table][$column]['help'],
          ));
        }

        // Reuse original short title.
        if (isset($data[$table][$column]['title short'])) {
          $data[$table]['arbitrary_sort_' . $column]['title short'] = t('@title', array(
            '@title' => $data[$table][$column]['title short'],
          ));
        }

        // Add the new handler itself.
        $data[$table]['arbitrary_sort_' . $column]['sort']['handler'] = 'views_arbitrary_sort_handler_sort_arbitrary';
      }
    }
  }
}
