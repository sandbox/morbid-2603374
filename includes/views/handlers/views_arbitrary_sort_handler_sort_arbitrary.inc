<?php
/**
 * @file
 * Definition of views_arbitrary_sort_handler_sort_arbitrary.
 */

/**
 * Arbitrary sort handler which accepts a list of values to sort.
 *
 * @ingroup views_sort_handlers
 */
class views_arbitrary_sort_handler_sort_arbitrary extends views_handler_sort {

  /**
   * Called to add the sort to a query.
   */
  public function query() {
    // Do not modify query if sorting values are missing.
    if ($this->options['arbitrary_sort_values'] === '') {
      return;
    }

    $sort_count = &drupal_static('views_arbitrary_sort_count', 0);
    $sort_count++;

    $this->ensure_my_table();

    // Get array of arbitrary sorting values.
    $arbitrary_sort_values = $this->get_sort_vals($this->options['arbitrary_sort_values']);

    // Begin CASE statement.
    $case = 'CASE';

    // Set up query fragments for match type.
    $prefix = $suffix = '';
    switch ($this->options['arbitrary_sort_match_type']) {
      case 'begins':
        $operator = ' LIKE ';
        $suffix = '%';
        break;

      case 'contains':
        $operator = ' LIKE ';
        $prefix = $suffix = '%';
        break;

      case 'exact':
      default:
        $operator = ' = ';
        break;
    }

    // Set up query case sensitivity.
    $binary = '';
    if ($this->options['arbitrary_sort_case_sensitive'] == 1) {
      $binary = 'BINARY ';
    }

    // Initiate variables for use in loop.
    $weight = 1;
    $arbitrary_sort_args = array();

    // Add each sorting value to the CASE statement as a condition.
    foreach ($arbitrary_sort_values as $sort_value) {
      // Create placeholder name for current value.
      $placeholder = ':arbitrary_sort_' . $sort_count . '_value_' . $weight;

      // Add condition to CASE statement.
      $case .= ' WHEN ' .
               $binary .
               $this->table_alias . '.' . $this->real_field .
               $operator .
               $placeholder .
               ' THEN ' . $weight;

      // Add sort value to placeholder arguments array.
      $arbitrary_sort_args[$placeholder] = $prefix . $sort_value . $suffix;

      $weight++;
    }

    // Terminate CASE statement.
    $case .= ' ELSE ' . $weight . ' END';

    // Add CASE statement to query.
    $alias = $this->query->add_field(NULL, $case, 'arbitrary_sort_' . $sort_count, array('placeholders' => $arbitrary_sort_args));
    $this->query->add_orderby($this->table_alias, NULL, $this->options['order'], $alias);
  }

  /**
   * Define handler options.
   *
   * @return array $options
   *   An array of handler options with default values.
   */
  public function option_definition() {
    $options = parent::option_definition();

    // Add arbitrary sorting definitions.
    $options['arbitrary_sort_match_type'] = array('default' => 'exact');
    $options['arbitrary_sort_case_sensitive'] = array('default' => 0);
    $options['arbitrary_sort_values'] = array('default' => '');

    return $options;
  }

  /**
   * Shortcut to display the value form.
   */
  public function show_sort_form(&$form, &$form_state) {
    parent::show_sort_form($form, $form_state);

    $form['order']['#title'] = t('Sorting order');

    // Add radios for selecting match type.
    $form['arbitrary_sort_match_type'] = array(
      '#type' => 'radios',
      '#title' => t('Match type'),
      '#default_value' => $this->options['arbitrary_sort_match_type'],
      '#options' => array(
        'exact' => t('Exact match'),
        'contains' => t('Contains'),
        'begins' => t('Starts with'),
      ),
    );

    // Add checkbox for enabling case sensitivity.
    $form['arbitrary_sort_case_sensitive'] = array(
      '#type' => 'checkbox',
      '#title' => t('Case sensitive sorting'),
      '#default_value' => $this->options['arbitrary_sort_case_sensitive'],
    );

    // Add textarea for configuring arbitrary sorting values.
    $form['arbitrary_sort_values'] = array(
      '#type' => 'textarea',
      '#title' => t('Sorting values'),
      '#description' => t('Enter a list of values representing your arbitrary sorting order, separated by new lines. Non-matching values will be sorted to the top or bottom of the list (depending on sorting order). Values must not be entered more than once.'),
      '#default_value' => $this->options['arbitrary_sort_values'],
      '#required' => TRUE,
    );
  }

  /**
   * Simple validate handler.
   */
  public function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);

    // Retrieve entered values and count occurrences.
    $arbitrary_sort_values = $this->get_sort_vals($form_state['values']['options']['arbitrary_sort_values']);
    $value_counts = array_count_values($arbitrary_sort_values);

    // Check for duplicate values.
    $dupe_found = FALSE;
    foreach ($value_counts as $value => $count) {
      if ($count > 1) {
        $dupe_found = TRUE;
        $dupes[] = $value;
      }
    }

    // Display an error if duplicates are found.
    if ($dupe_found) {
      sort($dupes);
      form_error($form['arbitrary_sort_values'], t('Duplicate sorting values found: %dupes.', array('%dupes' => implode(', ', $dupes))));
    }
  }

  /**
   * Helper function to extract sorting values from textarea input.
   *
   * @param string $input
   *   The inputted text from which to extract individual values.
   *
   * @return array
   *   An array containing a value for each line of input.
   */
  private function get_sort_vals($input) {
    // Trim whitespace and split on line breaks.
    return preg_split('/\r\n|[\r\n]/', trim($input));
  }

}
