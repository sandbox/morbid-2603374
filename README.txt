
CONTENTS OF THIS FILE
---------------------

 * Summary
 * Dependencies
 * Installation
 * Usage


SUMMARY
-------

Allows sorting of Views results in an arbitrary order based on a list of
specific values defined within the view.


DEPENDENCIES
------------

1. Views


INSTALLATION
------------

1. Copy this views_arbitrary_sort/ directory to your sites/all/modules
   directory.

2. Enable the module.


Usage
-----

1. Create or edit a view.

2. Add a sort to the view from the "Arbitrary Sort" group.

3. Select the sort order, match type and case sensitivity you wish to use.

4. Enter the values you wish to sort in the order you want them sorted,
   with each value on a new line.

5. Save the view.

Note: views_arbitrary_sort can only sort those fields for which Views' generic
sort handler (views_handler_sort) is normally used. Fields which use other
sort handlers are not currently supported (although this may change in the
future).
